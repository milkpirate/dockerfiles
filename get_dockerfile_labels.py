#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Get labels from a Dockerfile.

Usage:
  get_dockerfile_labels.py [ -f <dockerfile> ] <key>
  get_dockerfile_labels.py -v | -h

Arguments:
  <key>                                 Key to retrieve the value from.

Options:
  -f <dockerfile> --file=<dockerfile>   File to use. [default: ./Dockerfile]
  -h --help                             Show this screen.
  -v --version                          Show version.

© 2018 Paul Schroeder
"""

import docopt
import dockerfile_parse
import pathlib

version = "1.0.0"

args = docopt.docopt(
    __doc__,
    help=True,
    version=version
)

dockerfile = pathlib.Path(args['--file'])
dfp = dockerfile_parse.DockerfileParser()
dfp.content = dockerfile.read_text()
key = args['<key>']
val = dfp.labels.get(key, None)
print(val)
