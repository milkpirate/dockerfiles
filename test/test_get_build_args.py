#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import os

from conftest import \
    load_module_under_test_from, \
    run_cmd, \
    test_files_path

script_under_test = './get_build_args.py'


def test_process_args_ok():
    os.environ['DEF'] = "def"
    os.environ['GHI'] = "ghi"

    test_file = test_files_path / "build_args_ok.yaml"

    proc = run_cmd(
      f'{script_under_test} {test_file}',
      env=os.environ
    )
    print(proc)
    assert proc.returncode == 0
    assert proc.stdout == "--build-arg ABC=abc DEF=def GHI=ghi JKL\n"
    assert proc.stderr == ""


def test_process_args_nok():
    proc = run_cmd(f'{script_under_test} /non/exist')
    print(proc)
    assert proc.returncode == 0
    assert proc.stdout == ""
    assert "No such file or directory" in proc.stderr


def test_eprint(capsys):
    load_module_under_test = load_module_under_test_from('./get_build_args.py')
    load_module_under_test.eprint("test")
    captured = capsys.readouterr()
    assert captured.out == ""
    assert captured.err == "test\n"
