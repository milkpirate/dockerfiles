#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

from conftest import run_cmd, test_files_path

script_under_test = './get_dockerfile_labels.py'


def test_get_key():
    dockerfile = test_files_path / 'Dockerfile_with_labels'

    proc = run_cmd(f'{script_under_test} -f {dockerfile} org.opencontainers.image.authors')
    print(proc)
    assert proc.returncode == 0
    assert proc.stdout == "some author\n"
    assert proc.stderr == ""

    proc = run_cmd(f'{script_under_test} --file {dockerfile} org.opencontainers.image.title')
    print(proc)
    assert proc.returncode == 0
    assert proc.stdout == "some title\n"
    assert proc.stderr == ""

