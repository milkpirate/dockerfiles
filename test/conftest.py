#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import importlib.util
import os
import pathlib
import subprocess

test_files_path = pathlib.Path('test/data/')


def run_cmd(cmd, env=os.environ):
    return subprocess.run(
        cmd,
        env=env,
        capture_output=True,
        text=True,
        shell=True,
    )


def load_module_under_test_from(module_path):
    module_path = pathlib.Path(module_path).resolve()
    spec = importlib.util.spec_from_file_location(".", module_path)
    module_under_test = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module_under_test)
    return module_under_test
