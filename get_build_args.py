#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Reads environment variables from a flat yaml file, if keys without value are
found in the current environment, the value from it will be used.

The output can be supplied to the docker build option --build-args.

Usage:
  get_dockerfile_labels.py <env_file>
  get_dockerfile_labels.py -v | -h

Options:
  -h --help                         Show this screen.
  -v --version                      Show version.

© 2018 Paul Schroeder
"""

import docopt
import os
import sys
import yaml

version = "1.0.0"


def main(argv=sys.argv[1:]):
    args = docopt.docopt(
        __doc__,
        argv=argv,
        help=True,
        version=version
    )

    try:
        with open(args['<env_file>']) as env_yaml:
            env_vars = yaml.load(env_yaml, Loader=yaml.SafeLoader)
    except FileNotFoundError as exp:
        eprint(exp)
        exit(0)

    build_args = "--build-arg"
    for key, val in env_vars.items():
        if not val:
            val = os.environ.get(key, None)

        build_args += f' {key}'
        build_args += f"={val}" if val else ""

    print(build_args)


def eprint(*args, **kwargs) -> None:
    """
    Like the usual ``print()`` but to stderr.

    :param args: Same as of ``print()``.
    :param kwargs: Dito.

    >>> eprint("test")
    """
    print(*args, file=sys.stderr, **kwargs)


if __name__ == "__main__":
    main()
