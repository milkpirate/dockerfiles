#!/bin/sh

readonly __dir="$(dirname "$0")"

git clone "https://oauth2:${TIND3R_GITLAB_ACCESS}@gitlab.com/milkpirate/tind3r" "$__dir/tind3r"
